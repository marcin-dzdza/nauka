<?php
//co zrobić, żeby dodać blok do istniejącej już strony?
//źródło: http://cookie-code.net/magento-2/custom-block-store-configuration-options-magento-2/

/*
1. znajdź nazwę layoutu, np.
każda strona: view/frontend/layout/default.xml
strona główna: cms_index_index.xml
karta produktu: catalog_product_view.xml
*/

// view/frontend/layout/default.xml
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceContainer name="content">
            <block class="Marcin\CustomConfig\Block\FirstBlock" name="firstblock" template="firstblock.phtml" /> //tylko tu coś zmieniasz
        </referenceContainer>
    </body>
</page>

// view/frontend/templates/firstblock.phtml
<div style="background: #ff0; padding: 50px; margin: 0 auto; text-align: center;">
    <h1>On every page</h1>
</div>

// Block/FirstBlock.php
// piszesz cokolwiek, możesz utworzyć pustą klasę z namespacem