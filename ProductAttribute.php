<?php

/*
tu jest na pewno sprawdzone jądro tej funkcji, ale brakuje kontekstu:
https://stackoverflow.com/questions/39137857/custom-product-attribute-not-showing-in-admin-catalog-section-magento-2-1
tu zajrzyj do kontekstu
https://magento.stackexchange.com/questions/162113/magento-2-add-product-attribute-programmatically
dodatkowo musisz dodać admin ui
 */

//InstallData.php lub UpgradeData.php (tutaj upgrade)

namespace Marcin\CustomConfig\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Eav\Setup\EavSetupFactory;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Product setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'new_packaging_cost',
            [
                'group' => 'General',
                'type' => 'int', //od tego zależy tabela eav w db. możliwe też varchar, decimal i inne. Aby mieć boolean, tu musi być int
                'backend' => '',
                'frontend' => '',
                'label' => 'New Packaging Cost',
                'input' => 'text', //także dla liczb. Inne możliwości to np. select, textarea
                'class' => '',
                'source' => '',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL, //od tego zależy, czy będzie się dało ustawić różne wartości dla różnych sklepów
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable' //możliwe, że zbędne?
            ]
        );

        //wpis zostanie dodany co najmniej do tabeli "eav_attribute"
        //konkretna wartość jest dodawana m.in do catalog_product_entity_int/decimal/itd.


        //aby zmienić cechy atrybutu później:
        if (version_compare($context->getVersion(), '1.8', '<')) {
            $entityType = $eavSetup->getEntityTypeId('catalog_product');
            $eavSetup->updateAttribute($entityType, 'packaging_cost', 'frontend_label','Packaging Cost Nowe');
        }

        $installer->endSetup();
    }
}

//adminhtml/ui_component/product_form.xml
<?xml version="1.0" encoding="UTF-8"?>
<form xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Ui:etc/ui_configuration.xsd">
    <fieldset name="general">
        <field name="new_packaging_cost">
            <argument name="data" xsi:type="array">
                <item name="config" xsi:type="array">
                    <item name="sortOrder" xsi:type="number">60</item>
                    <item name="dataType" xsi:type="string">number</item>
                    <item name="formElement" xsi:type="string">input</item>
                    <item name="label" xsi:type="string" translate="true">New packaging cost </item>
                </item>
            </argument>
        </field>
    </fieldset>
</form>

//aby uzyskać wartość tego atrybutu w blocku
<?php
namespace Marcin\CustomConfig\Block;

use \Magento\Framework\View\Element\Template\Context;
use \Marcin\BoolAttribute\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Catalog\Api\CategoryRepositoryInterface;

class ThirdBlock extends \Magento\Framework\View\Element\Template
{
    protected $productRepository;

    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        array $data = []
    )
    {
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);
    }
}

$product = $this->productRepository->get('24-MB01');
$value = $product->getData('new_packaging_cost');


// jeśli zrobisz attribute int, dodasz jakąś wartość, a potem zmienisz na decimal, to nowo ustawione wartości będą ok, ale te stare będą się ciągnąć jako problem. Przy edycji będzie się pobierać stara wartość z tabeli int i do niej też będzie się zapisywać edycja, czyli nic nir zrobisz, żeby móc podać decimala. Takie wpisy trzeba od razu w jakiś sposób migrować do dobrej tabeli albo usuwać
